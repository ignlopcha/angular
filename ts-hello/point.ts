export class Point3 { //avec le mot exort on transforme le fichier en module
    constructor(private _x?: number, private _y?: number) { //on declare l' acces modifier dans le constructeur, bas besoin de declarer les attributs avant et le this apres


    }

    draw() {
        console.log("X: " + this._x + " Y: " + this._y);
    }

    get x() { //On transforme le get en property
        return this._x;
    }

    set x(value) { //On transforme le set en property
        if (value < 0) throw new Error("value")
        this._x = value;

    }

}

