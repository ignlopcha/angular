import { Point3 } from "./point"
let numero = 5; // numero type number

let message1;

message1 = "abc"; //message type string

let endsWithC = (<string>message1).endsWith("c"); // on doit le caster

let endsWithC1 = (message1 as string).endsWith("c"); // on doit le caster

//Interface

interface Point {
    x: number,
    y: number
}

let drawPoint = (point: Point) => {
    //..
}

drawPoint({
    x: 1,
    y: 2
})

//Class

class Point2 { //on regroupe tout dans le meme element COHESION
    private x: number;
    private y: number;

    constructor(x?: number, y?: number) { //on fait tous les paramtres optional, si on fait un optional o doit tous faire
        this.x = x;                      //sur ts on peut pas faire pluisieurs constructeurs, avec l'optional c'est la façon de le faire
        this.y = y;
    }
    draw() {
        console.log("X: " + this.x + " Y: " + this.y);
    }
}

let point2 = new Point2(1, 2);

point2.draw();

//Properties 
/* class Point3 {
    constructor(private _x?: number, private _y?: number) { //on declare l' acces modifier dans le constructeur, bas besoin de declarer les attributs avant et le this apres

    }
    draw() {
        console.log("X: " + this._x + " Y: " + this._y);
    }

    getX() {
        return this.x;
    } 

     setX(value) {
        if (value<0) throw new Error ("value") //revoir sintaxe simplifié
        this.x=value; 
this.x = value
    }

     get x() { //On transforme le get en property
        return this._x;
    }

    set x(value) { //On transforme le set en property
        if (value < 0) throw new Error("value")
        this._x = value;
        
    }

} */

let point3 = new Point3(1, 2);

let x = point3.x; // o a acces a x sans passer par la methode
point3.x = 10;
