var numero = 5; // numero type number
var message1;
message1 = "abc"; //message type string
var endsWithC = message1.endsWith("c"); // on doit le caster
var endsWithC1 = message1.endsWith("c"); // on doit le caster
var drawPoint = function (point) {
    //..
};
drawPoint({
    x: 1,
    y: 2
});
//Class
var Point2 = /** @class */ (function () {
    function Point2() {
    }
    Point2.prototype.draw = function () {
        console.log("X: " + this.x + "Y: " + this.y);
    };
    Point2.prototype.getDistance = function (another) {
        //..
    };
    return Point2;
}());
var point = new Point2();
point.x = 1;
point.y = 2;
point.draw();
