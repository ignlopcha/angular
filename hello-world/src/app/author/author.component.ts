import { AppComponent } from './../app.component';
import { AuthorService } from './../author.service';
import { Component } from '@angular/core';


@Component({
  selector: 'authors',
  //templateUrl: "./author.component.html",

  //put in author.component.html file
  template: `    

  <ul>
          <li *ngFor="let author of authors">
          {{ author }}
          </li>
  </ul>
  `
  //styleUrls: ['./author.component.sass']
})


export class AuthorComponent {

  authors: any;

  constructor(service: AuthorService) {
    this.authors = service.getAuthors();
  }



}
