
import { Component } from "@angular/core";//decorator to attach to a class and make a class a component
import { CoursesService } from "./courses.service";

@Component({ //this function takes an argument as parameter
    selector: "courses",  //anywhere we have this selector, angul ar is going to render the template
    //template: "<h2>Courses</h2>" // what  we render / see

    //template: "<h2>{{'Title: ' + title}}</h2>" // Interpolation
    //template: "<h2>{{'Title: ' + title}}</h2>"//video 5
    //template: "<h2>{{ getTitle() }}</h2>"// video 5

    //video 6
    //*ngFor = directive, modifies an element from the DOM
    //we consume directly from our componnent is not the more common thing
    //Ussually we are going to consume it from an HTTP service
    template: `    
    <h2>{{ title }}</h2>
    <ul>
            <li *ngFor="let course of courses">
            {{ course }}
            </li>

            <button class="btn btn-primary">Save</button>
    </ul>
    `
})

export class CoursesComponent {
    //register component in a module, we add it to app.modules.ts
    title = "List of Courses";
    /* video 5
    getTitle() {
        return this.title;
    } */
    // courses = ["course1", "course2", "course3"]; //video 6

    //video 8 Dependency Injection
    courses: any;

    constructor(service: CoursesService) { // insert a dependency de type CoursesService, when the object is created it creates an instance of CoursesService and passes it as parameter 
        // let service = new CoursesService();
        this.courses = service.getCourses();
    }


}