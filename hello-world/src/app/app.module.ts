import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoursesComponent } from './courses.component';
import { CourseComponent } from './course/course.component'; // atuomatically imported
import { CoursesService } from './courses.service';
import { AuthorComponent } from './author/author.component';
import { AuthorService } from './author.service';

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent, // added component
    CourseComponent, AuthorComponent // added component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [ //register dependencies
    CoursesService, //when angular is going to create an instance of this component first is going to instantiate it dependencies
    //the inject this dependency in the constructor of the class, DEPENDENCY INJECTION
    //CoursesService is only instantiate once (SINGLETON PATTERN) and after injected
    AuthorService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
